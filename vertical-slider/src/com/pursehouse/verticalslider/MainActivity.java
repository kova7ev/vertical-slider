package com.pursehouse.verticalslider;

import android.support.v7.app.ActionBarActivity;

import com.pursehouse.verticalslider.chart.ChartHolder;

import android.opengl.GLSurfaceView;
import android.os.Bundle;

public class MainActivity extends ActionBarActivity {

	private GLSurfaceView mView;

	public MainActivity() {
		ApplicationContext.Context = this;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Create a GLSurfaceView instance and set it
		// as the ContentView for this Activity
		mView = new ChartHolder(this);
//		mView = new SliderView(this);
		setContentView(mView);
	}

	@Override
	protected void onPause() {
		super.onPause();
		// The following call pauses the rendering thread.
		// If your OpenGL application is memory intensive,
		// you should consider de-allocating objects that
		// consume significant memory here.
		mView.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// The following call resumes a paused rendering thread.
		// If you de-allocated graphic objects for onPause()
		// this is a good place to re-allocate them.
		mView.onResume();
	}
}