/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pursehouse.verticalslider.text;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import com.pursehouse.verticalslider.ApplicationContext;
import com.pursehouse.verticalslider.SliderRenderer;
import com.pursehouse.verticalslider.chart.utils.GLUtil;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.opengl.GLES20;
import android.util.Log;

/**
 * A two-dimensional square for use as a drawn object in OpenGL ES 2.0.
 */
public class Text {

	public static final String TAG = "Text";

	private final String vertexShaderCode = ""
			+
			// This matrix member variable provides a hook to manipulate
			// the coordinates of the objects that use this vertex shader
			"uniform mat4 uMVPMatrix;" + "attribute vec4 aPosition;"
			+ "attribute vec2 aTexCoords;" + "varying vec2 vTexCoords;"
			+ "void main(){" + "  vTexCoords = aTexCoords;"
			+ "  gl_Position = uMVPMatrix * aPosition;" + "}";

	private final String fragmentShaderCode = "" + "precision mediump float;"
			+ "uniform sampler2D uTexture;" + "uniform float uAlpha;"
			+ "varying vec2 vTexCoords;" + "void main(){"
			+ "  gl_FragColor = texture2D(uTexture, vTexCoords);"
			// + "  gl_FragColor.a = uAlpha;"
			+ "}";

	private FloatBuffer vertexBuffer;
	private ShortBuffer drawListBuffer;
	private FloatBuffer textureCoordsBuffer;
	private static int mProgram;
	private static boolean mProgramCreated;
	private int mPositionHandle;
	// private int mColorHandle;
	private int mAlphaHandle;
	private int mTextureHandle;
	private int mTextureCoordsHandle;
	private int mMVPMatrixHandle;
	private int mTextureHandles[];

	// number of coordinates per vertex in this array
	static final int COORDS_PER_VERTEX = 3;
	static final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
	// S, T (or X, Y)
	static final int COORDS_PER_TEXTURE_VERTEX = 2;
	static final int TEXTURE_VERTEX_STRIDE_BYTES = COORDS_PER_TEXTURE_VERTEX
			* GLUtil.BYTES_PER_FLOAT;

	float vertexCoords[];

	float textureCoords[] =
	 { 0, 0, // top left
	 0, 1, // bottom left
	 1, 1, // bottom right
	 1, 0, // top right
	 };

	private final short drawOrder[] = { 0, 1, 2, 0, 2, 3 }; // order to draw
															// vertices

	// float color[] = { 0.2f, 0.709803922f, 0.898039216f, 1.0f };

	private static float mWidth;
	private static float mHeight;

	private String mText = "OpenGL text";
	private float mFontSize = 14;
	private String mFontType = "Roboto/Roboto-BlackItalic.ttf";
	private float mColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float mScale = 1;
	private float mPosX;
	private float mPosY;
	private boolean mInvalide;
	private Bitmap mBitmap;
	private float mBitmapWidth;
	private float mBitmapHeight;
	private boolean mPositionChanged;

	/**
	 * Sets up the drawing object data for use in an OpenGL ES context.
	 */
	public Text(float posX, float posY, String text) {
		mPosX = posX;
		mPosY = posY;
		mText = text;
		mInvalide = true;
		mPositionChanged = true;

		// // initialize vertex byte buffer for shape coordinates
		// ByteBuffer bb = ByteBuffer.allocateDirect(
		// // (# of coordinate values * 4 bytes per float)
		// vertexCoords.length * 4);
		// bb.order(ByteOrder.nativeOrder());
		// vertexBuffer = bb.asFloatBuffer();
		// vertexBuffer.put(vertexCoords);
		// vertexBuffer.position(0);

		 //initialize byte buffer for the draw list
		 ByteBuffer dlb = ByteBuffer.allocateDirect(
		 // (# of coordinate values * 2 bytes per short)
		 drawOrder.length * 2);
		 dlb.order(ByteOrder.nativeOrder());
		 drawListBuffer = dlb.asShortBuffer();
		 drawListBuffer.put(drawOrder);
		 drawListBuffer.position(0);
		//
		 textureCoordsBuffer = GLUtil.asFloatBuffer(textureCoords);

		// initialize textures
		// mTextureHandles = new int[1];
		// Bitmap background =
		// GLUtil.getResourceById(ApplicationContext.Context,
		// R.drawable.purple_gradient);
		// mTextureHandles[0] = GLUtil.loadTexture(background);

		if (!mProgramCreated) {

			// prepare shaders and OpenGL program
			int vertexShader = SliderRenderer.loadShader(
					GLES20.GL_VERTEX_SHADER, vertexShaderCode);
			int fragmentShader = SliderRenderer.loadShader(
					GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

			mProgram = GLES20.glCreateProgram(); // create empty OpenGL Program
			GLES20.glAttachShader(mProgram, vertexShader); // add the vertex
															// shader to program
			GLES20.glAttachShader(mProgram, fragmentShader); // add the fragment
																// shader to
																// program
			GLES20.glLinkProgram(mProgram); // create OpenGL program executables
			mProgramCreated = true;
		}
	}

	/**
	 * Encapsulates the OpenGL ES instructions for drawing this shape.
	 *
	 * @param mvpMatrix
	 *            - The Model View Project matrix in which to draw this shape.
	 */
	public void draw(float[] mvpMatrix) {
		// Add program to OpenGL environment
		GLES20.glUseProgram(mProgram);

		// get handle to fragment shader's vColor member
		// mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
		// Set color for drawing the triangle
		// GLES20.glUniform4fv(mColorHandle, 1, color, 0);

		// Set up texture stuff

		if (mInvalide) {
			Log.e(TAG, "recreate bitmap");
			//mBitmap = createBitmap();
			mBitmap = TextRenderer.getBitmap();// createBitmap();

			Rect rect = TextRenderer.getTextureCoords(mText);

			if (rect == null) {
				return;
			}

			float left = 1.0f * rect.left / mBitmap.getWidth();
			float top = 1.0f * rect.top / mBitmap.getHeight();
			float right = 1.0f * rect.right / mBitmap.getWidth();
			float bottom = 1.0f * rect.bottom / mBitmap.getHeight();

			Log.w(TAG, String.format("%s: [%f, %f, %f, %f]", mText, left, top, right, bottom));
			
			textureCoords = new float[] { left, top, // top left
					left, bottom, // bottom left
					right, bottom, // bottom right
					right, top // top right
			};

			// initialize vertex byte buffer for shape coordinates
			ByteBuffer bb = ByteBuffer.allocateDirect(
			// (# of coordinate values * 4 bytes per float)
					textureCoords.length * 4).order(ByteOrder.nativeOrder());

			textureCoordsBuffer = bb.asFloatBuffer();
			textureCoordsBuffer.put(textureCoords);
			textureCoordsBuffer.position(0);
		}

		if (mPositionChanged) {
//			float width = TextRenderer.getWidth() / (0.5f * mWidth);
//			float height = TextRenderer.getHeight() / (0.5f * mHeight);
			
			//TODO:
			float width = 0;
			float height = 0;

			vertexCoords = new float[] { mPosX, mPosY + mScale * height / 2,
					0.0f, // top
							// left
					mPosX, mPosY - mScale * height / 2, 0.0f, // bottom left
					mPosX - mScale * width, mPosY - mScale * height / 2, 0.0f, // bottom
																				// right
					mPosX - mScale * width, mPosY + mScale * height / 2, 0.0f }; // top
																					// right

			// initialize vertex byte buffer for shape coordinates
			ByteBuffer bb = ByteBuffer.allocateDirect(
			// (# of coordinate values * 4 bytes per float)
					vertexCoords.length * 4);
			bb.order(ByteOrder.nativeOrder());
			vertexBuffer = bb.asFloatBuffer();
			vertexBuffer.put(vertexCoords);
			vertexBuffer.position(0);

			mPositionChanged = false;
		}

		// get handle to vertex shader's vPosition member
		mPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");

		// Enable a handle to the triangle vertices
		GLES20.glEnableVertexAttribArray(mPositionHandle);

		// Prepare the triangle coordinate data
		GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);

		if (mInvalide) {
			mTextureHandles = new int[1];
			mTextureHandles[0] = GLUtil.loadTexture(mBitmap);
			mInvalide = false;
		}

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandles[0]);
		GLUtil.checkGlError("glBindTexture");

		mTextureCoordsHandle = GLES20.glGetAttribLocation(mProgram,
				"aTexCoords");
		SliderRenderer.checkGlError("glGetAttribLocation");

		GLES20.glEnableVertexAttribArray(mTextureCoordsHandle);
		SliderRenderer.checkGlError("glEnableVertexAttribArray");

		GLES20.glVertexAttribPointer(mTextureCoordsHandle,
				COORDS_PER_TEXTURE_VERTEX, GLES20.GL_FLOAT, false,
				TEXTURE_VERTEX_STRIDE_BYTES, textureCoordsBuffer);
		SliderRenderer.checkGlError("glVertexAttribPointer");

		mTextureHandle = GLES20.glGetUniformLocation(mProgram, "uTexture");
		SliderRenderer.checkGlError("glGetUniformLocation");

		GLES20.glUniform1i(mTextureHandle, 0);
		SliderRenderer.checkGlError("glUniform1i");

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		SliderRenderer.checkGlError("glActiveTexture");

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandles[0]);
		GLUtil.checkGlError("glBindTexture");
		// Set the alpha
		mAlphaHandle = GLES20.glGetUniformLocation(mProgram, "uAlpha");
		SliderRenderer.checkGlError("glGetUniformLocation");
		GLES20.glUniform1f(mAlphaHandle, 1.0f);
		SliderRenderer.checkGlError("glUniform1f");

		// get handle to shape's transformation matrix
		mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
		SliderRenderer.checkGlError("glGetUniformLocation");

		// Apply the projection and view transformation
		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
		SliderRenderer.checkGlError("glUniformMatrix4fv");

		// Draw the square
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
				GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPositionHandle);

		// GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		// GLUtil.checkGlError("glDeleteTextures");
	}

	public void dispose() {
		GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		GLUtil.checkGlError("glDeleteTextures");
	}

	private Bitmap createBitmap() {
		Resources resources = ApplicationContext.Context.getResources();
		float density = resources.getDisplayMetrics().density;

		AssetManager asset = ApplicationContext.Context.getAssets();
		Typeface tf = Typeface.createFromAsset(asset,
				String.format("fonts/%s", mFontType));

		// new antialised Paint
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		// text typeface
		paint.setTypeface(tf);
		// text color
		paint.setColor(Color.argb((int) (mColor[3] * 255),
				(int) (mColor[0] * 255), (int) (mColor[1] * 255),
				(int) (mColor[2] * 255)));
		// text size in pixels
		paint.setTextSize((int) (mFontSize * density));
		// text shadow
		paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

		// draw text to the Canvas center
		Rect bounds = new Rect();
		paint.getTextBounds(mText, 0, mText.length(), bounds);

		// Create an empty, mutable bitmap
		mBitmapWidth = bounds.width() * 2;
		mBitmapHeight = bounds.height();

		// text size in pixels
		paint.setTextSize((int) (mFontSize * density * 10));
		// text shadow
		paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

		// draw text to the Canvas center
		bounds = new Rect();
		paint.getTextBounds(mText, 0, mText.length(), bounds);
		Bitmap bitmap = Bitmap.createBitmap((int) (bounds.width() * 2),
				(int) (bounds.height()), Bitmap.Config.ARGB_8888);

		bitmap.eraseColor(Color.argb(0, 0, 0, 0));
		// get a canvas to paint over the bitmap
		Canvas canvas = new Canvas(bitmap);

		// Paint paintTransparent = new Paint();
		// paint.setAlpha(60); //you can set your transparent value here
		// canvas.drawBitmap(bitmap, 0, 0, paintTransparent);

		int x = 0;
		int y = bitmap.getHeight();

		canvas.drawText(mText, x, y, paint);
		return bitmap;
	}

	public static void onSurfaceChanged(int width, int height) {
		mWidth = width;
		mHeight = height;
	}

	public void setPositionX(float x) {
		mPosX = x;
		mPositionChanged = true;
	}
	
	public float getPositionX(){
		return mPosX;
	}

	public void setPositionY(float y) {
		mPosY = y;
		mPositionChanged = true;
	}
	
	public float getPositionY(){
		return mPosY;
	}

	public void setText(String text) {
		mText = text;
		mInvalide = true;
	}
	
	public String getText(){
		return mText;
	}

	public void setScale(float scale) {
		mScale = scale;
		mPositionChanged = true;
	}
	
	public float getScale(){
		return mScale;
	}
	
	public static float getScreenWidth(){
		return mWidth;
	}
	
	public static float getScreenHeight(){
		return mHeight;
	}
}