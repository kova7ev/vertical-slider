package com.pursehouse.verticalslider.text;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.pursehouse.verticalslider.ApplicationContext;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Environment;
import android.util.Log;

public class TextRenderer {
	public static final String TAG = "TextRenderer";
	
	private static Bitmap mBitmap;
	private static String mKeys;
	private static float mWidth;
	private static float mHeight;
	private static float mScale;
	private static Map<String, Rect> mPosition;
	
	static{
//		mKeys = "0123456789.%������������ ";
//		mKeys = "0127claims";
		mKeys = "01234567890.% �������������������������������������Ũ��������������������������";
		mWidth = 0;
		mHeight = 0;
		mScale = 1.0f;
		mPosition = new HashMap<String, Rect>();
	}
	
	public static void initialize(String fontType, float fontSize, float[] color) {
		Resources resources = ApplicationContext.Context.getResources();
		float density = resources.getDisplayMetrics().density;

		AssetManager asset = ApplicationContext.Context.getAssets();
		Typeface tf = Typeface.createFromAsset(asset, String.format("fonts/%s", fontType));

		//for rect
		Paint paintStrokeOnly = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStrokeOnly.setColor(Color.argb(255, 0, 0, 0));
		paintStrokeOnly.setStrokeWidth(2);
		paintStrokeOnly.setStyle(Paint.Style.STROKE);
		
		// new antialised Paint
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		// text typeface
		paint.setTypeface(tf);
		// text color
//		paint.setColor(Color.argb((int) (color[3] * 255),
//				(int) (color[0] * 255), (int) (color[1] * 255),
//				(int) (color[2] * 255)));
		
		paint.setColor(Color.argb(255, 0, 0, 0));

		// text size in pixels
		// using scale for case if texture will be upscaled
		paint.setTextSize((int) (fontSize * density * mScale));
		// text shadow
		paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
		
		for (int i = 0; i < mKeys.length(); i++){
			// draw text to the Canvas center
			String text = mKeys.substring(i, i+1);
			Rect bounds = new Rect();
			paint.getTextBounds(text, 0, text.length(), bounds);
			int width = Math.abs(bounds.left) + Math.abs(bounds.right);
			int height = Math.abs(bounds.bottom) + Math.abs(bounds.top);
			
			mWidth = Math.max(mWidth, width);
			mHeight = Math.max(mHeight, height);
		}
		
//		mWidth *= 1.1;
				
//		// draw text to the Canvas center
//		//paint.getTextBounds(mText, 0, mText.length(), bounds);
		int w = 2;
		while (w < mWidth * mScale * mKeys.length()){
			w *= 2;
		}
		
		int h = 2;
		while (h < mHeight * mScale){
			h *= 2;
		}
		
//		Log.w(TAG, String.format("TEXTURE SIZE: [%dx%d]", w, h));
		
//		mBitmap = Bitmap.createBitmap((int) (mWidth * mScale), (int)( 2.0f * mHeight * mScale * mKeys.length()), Bitmap.Config.ARGB_8888);
		mBitmap = Bitmap.createBitmap(w, h*3, Bitmap.Config.ARGB_8888);
//		mBitmap.eraseColor(Color.argb(255, 255, 255, 255));
		mBitmap.eraseColor(Color.argb(0, 0, 0, 0));
		// get a canvas to paint over the bitmap
		Canvas canvas = new Canvas(mBitmap);

		int currentX = 0;
		for (int i = 0; i < mKeys.length(); i++){
			String text = mKeys.substring(i, i+1);
			
			Rect bounds = new Rect();
			paint.getTextBounds(text, 0, text.length(), bounds);
			
			int width = Math.abs(bounds.left) + Math.abs(bounds.right);
			int height = Math.abs(bounds.bottom) + Math.abs(bounds.top);
			
			int x = currentX + (width / 2);
//			int y = (int) ((height / 2) - ((paint.descent() + paint.ascent()) / 2)) + height; 
			int y = height + height/2;
			
			int convexHullX = currentX+width/2;
			int convexHullY = height-height/2 - 5;
			
//			if (text.equals("i"))
			{
				canvas.drawText(text, x, y, paint);
//				canvas.drawRect(convexHullX, convexHullY, convexHullX+width, convexHullY+height+10, paintStrokeOnly);
			}
			
			mPosition.put(text, new Rect(convexHullX, convexHullY, convexHullX+width, convexHullY+height+10));
			currentX += width*1.5f;
		}
		
		FileOutputStream out = null;
		String downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
		try {
		    out = new FileOutputStream(String.format("%s/atlas.bmp", downloads));
		    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
		    // PNG is a lossless format, the compression factor (100) is ignored
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    try {
		        if (out != null) {
		            out.close();
		        }
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}
	}
	
	public static Bitmap getBitmap(){
		return mBitmap;
	}

	public static Rect getTextureCoords(String text){
		if (text == null || text.length() != 1 || !mKeys.contains(text)){
			return null;
		}
		
		return mPosition.get(text);
	}

//	public static float getWidth() {
//		return mWidth;
//	}
//	
//	public static float getHeight(){
//		return mHeight;
//	}
	
}