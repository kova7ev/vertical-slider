/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pursehouse.verticalslider.text;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;

import com.pursehouse.verticalslider.SliderRenderer;
import com.pursehouse.verticalslider.Stroke;
import com.pursehouse.verticalslider.chart.utils.GLUtil;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.opengl.GLES20;
import android.util.Log;

/**
 * A two-dimensional triangle for use as a drawn object in OpenGL ES 2.0.
 */
public class SimpleText {

	public static final String TAG = "Strokes";

	private final String vertexShaderCodeText = ""
			+
			// This matrix member variable provides a hook to manipulate
			// the coordinates of the objects that use this vertex shader
			"uniform mat4 uMVPMatrix;" + "attribute vec4 aPosition;"
			+ "attribute vec2 aTexCoords;" + "varying vec2 vTexCoords;"
			+ "void main(){" + "  vTexCoords = aTexCoords;"
			+ "  gl_Position = uMVPMatrix * aPosition;" + "}";

	private final String fragmentShaderCodeText = ""
			+ "precision mediump float;" + "uniform sampler2D uTexture;"
			+ "uniform float uAlpha;" + "varying vec2 vTexCoords;"
			+ "void main(){"
			+ "  gl_FragColor = texture2D(uTexture, vTexCoords);"
			// + "  gl_FragColor.a = uAlpha;"
			+ "}";

	private FloatBuffer vertexBufferText;
	private ShortBuffer drawListBufferText;
	private FloatBuffer textureCoordsBufferText;
	private static int mProgramText;
	// private static boolean mProgramCreated;
	private int mPositionHandleText;
	// private int mColorHandle;
	private int mAlphaHandleText;
	private int mTextureHandleText;
	private int mTextureCoordsHandleText;
	private int mMVPMatrixHandleText;
	private static int mTextureHandlesText[];

	private static Bitmap mBitmap;

	// number of coordinates per vertex in this array
	static final int COORDS_PER_VERTEX = 3;
	static final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
	// S, T (or X, Y)
	static final int COORDS_PER_TEXTURE_VERTEX = 2;
	static final int TEXTURE_VERTEX_STRIDE_BYTES = COORDS_PER_TEXTURE_VERTEX
			* GLUtil.BYTES_PER_FLOAT;
	/*
	 * static float triangleCoords[] = { // in counterclockwise order: 0.0f,
	 * 0.622008459f, 0.0f, // top -0.5f, -0.311004243f, 0.0f, // bottom left
	 * 0.5f, -0.311004243f, 0.0f // bottom right };
	 */

	private float vertexCoords[];
	private float textureCoords[];
	private short drawOrder[]; // order to draw
								// vertices

	// float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 0.0f };
	

	private List<Text> mLabels;
	

	private float mPosX;
	private float mPosY;
	private String mText;
	private float mScale;
	private float mFontSize;
	private float mColor[] = { 1.0f, 1.0f, 1.0f, 1.0f }; 

	public SimpleText(float posX, float posY, String text, float fontSize, float[] color) {
		mPosX = posX;
		mPosY = posY;
		mText = text;
		mScale = 1;
		mFontSize = fontSize;
		mColor = color;
		
		mLabels = new ArrayList<Text>();
		Text lable = new Text(mPosX, mPosY, mText);
		lable.setScale(mScale);
		mLabels.add(lable);

		// prepare shaders and OpenGL program
		int vertexShader = SliderRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
				vertexShaderCodeText);
		int fragmentShader = SliderRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
				fragmentShaderCodeText);

		mProgramText = GLES20.glCreateProgram(); // create empty OpenGL
													// Program
		GLES20.glAttachShader(mProgramText, vertexShader); // add the vertex
															// shader
		// to program
		GLES20.glAttachShader(mProgramText, fragmentShader); // add the
																// fragment
																// shader to
																// program
		GLES20.glLinkProgram(mProgramText); // create OpenGL program
											// executables
	}
	
	/**
	 * Encapsulates the OpenGL ES instructions for drawing this shape.
	 *
	 * @param mvpMatrix
	 *            - The Model View Project matrix in which to draw this shape.
	 */
	public void draw(float[] mvpMatrix) {
		drawLabels(mvpMatrix);
	}

	private void drawLabels(float[] mvpMatrix) {
		// Add program to OpenGL environment
		GLES20.glUseProgram(mProgramText);

		int numberCharacters = mText.replace(" ", "").length();
		
		vertexCoords = new float[numberCharacters * 4 * COORDS_PER_VERTEX];
		textureCoords = new float[numberCharacters * 4
				* COORDS_PER_TEXTURE_VERTEX];

		int index = 0;
		int vertexCoordOffset = 0;

		for (int i = 0; i < mLabels.size(); i++) {
			Text label = mLabels.get(i);
			if (label != null) {
				String text = label.getText();

				//float scale = 7;//font? 1;//label.getScale();
				float scale = mFontSize;
				Rect rect = TextRenderer.getTextureCoords(text.substring(0, 1));
				float left = 1.0f * rect.left / mBitmap.getWidth();
				float top = 1.0f * rect.top / mBitmap.getHeight();
				float right = 1.0f * rect.right / mBitmap.getWidth();
				float bottom = 1.0f * rect.bottom / mBitmap.getHeight();
				float convexHullWidth = rect.right-rect.left;
				float convexHullHeight = rect.bottom-rect.top;
				float width = 1.0f * convexHullWidth / mBitmap.getWidth();
				float height = 1.0f * convexHullHeight / mBitmap.getHeight();
				
				float posX = label.getPositionX() + (scale * width * label.getText().length() / 2);
				
				for (int j = 0; j < text.length(); j++) {
					String c = text.substring(j, j+1);
					
					// TODO: While support string with length == 1
					rect = TextRenderer.getTextureCoords(c);

					if (rect == null) {
						return;
					}
					
					if (c.equals(" ")){
						posX = posX - scale * width;
						continue;
					}

					left = 1.0f * rect.left / mBitmap.getWidth();
					top = 1.0f * rect.top / mBitmap.getHeight();
					right = 1.0f * rect.right / mBitmap.getWidth();
					bottom = 1.0f * rect.bottom / mBitmap.getHeight();

					textureCoords[index] = left;
					textureCoords[index + 1] = top;
					textureCoords[index + 2] = left;
					textureCoords[index + 3] = bottom;
					textureCoords[index + 4] = right;
					textureCoords[index + 5] = bottom;
					textureCoords[index + 6] = right;
					textureCoords[index + 7] = top;

					index += 8;
					
					convexHullWidth = rect.right-rect.left;
					convexHullHeight = rect.bottom-rect.top;
					float size = Math.max(mBitmap.getWidth(), mBitmap.getHeight());
					width = 1.0f * convexHullWidth / size;
					height = 1.0f * convexHullHeight / size;
					
//					float scale = 1;//label.getScale();
					float posY = label.getPositionY();

					vertexCoords[vertexCoordOffset] = posX;
					vertexCoords[vertexCoordOffset + 1] = posY + scale * height / 2;
					vertexCoords[vertexCoordOffset + 2] = 0.0f;

					vertexCoords[vertexCoordOffset + 3] = posX;
					vertexCoords[vertexCoordOffset + 4] = posY - scale * height / 2;
					vertexCoords[vertexCoordOffset + 5] = 0.0f;

					vertexCoords[vertexCoordOffset + 6] = posX - scale * width;
					vertexCoords[vertexCoordOffset + 7] = posY - scale * height / 2;
					vertexCoords[vertexCoordOffset + 8] = 0.0f;

					vertexCoords[vertexCoordOffset + 9] = posX - scale * width;
					vertexCoords[vertexCoordOffset + 10] = posY + scale * height / 2;
					vertexCoords[vertexCoordOffset + 11] = 0.0f;

					vertexCoordOffset += 12;
					posX = posX - scale * width;

				}

			}
		}

		// initialize vertex byte buffer for shape coordinates
		ByteBuffer bb = ByteBuffer.allocateDirect(
		// (# of coordinate values * 4 bytes per float)
				textureCoords.length * 4).order(ByteOrder.nativeOrder());

		textureCoordsBufferText = bb.asFloatBuffer();
		textureCoordsBufferText.put(textureCoords);
		textureCoordsBufferText.position(0);

		// initialize vertex byte buffer for shape coordinates
		bb = ByteBuffer.allocateDirect(
		// (# of coordinate values * 4 bytes per float)
				vertexCoords.length * 4);
		bb.order(ByteOrder.nativeOrder());
		vertexBufferText = bb.asFloatBuffer();
		vertexBufferText.put(vertexCoords);
		vertexBufferText.position(0);

		drawOrder = new short[numberCharacters * 6];
		index = 0;

		for (int i = 0; i < numberCharacters; i++) {
			// { 0, 1, 2, 0, 2, 3 }
			drawOrder[index] = (short) (i * 4);
			drawOrder[index + 1] = (short) (i * 4 + 1);
			drawOrder[index + 2] = (short) (i * 4 + 2);
			drawOrder[index + 3] = (short) (i * 4);
			drawOrder[index + 4] = (short) (i * 4 + 2);
			drawOrder[index + 5] = (short) (i * 4 + 3);

			index += 6;
		}

		// initialize byte buffer for the draw list
		ByteBuffer dlb = ByteBuffer.allocateDirect(
		// (# of coordinate values * 2 bytes per short)
				drawOrder.length * 2);
		dlb.order(ByteOrder.nativeOrder());
		drawListBufferText = dlb.asShortBuffer();
		drawListBufferText.put(drawOrder);
		drawListBufferText.position(0);

		// get handle to vertex shader's vPosition member
		mPositionHandleText = GLES20.glGetAttribLocation(mProgramText,
				"aPosition");

		// Enable a handle to the triangle vertices
		GLES20.glEnableVertexAttribArray(mPositionHandleText);

		// Prepare the triangle coordinate data
		GLES20.glVertexAttribPointer(mPositionHandleText, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, vertexStride, vertexBufferText);

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandlesText[0]);
		GLUtil.checkGlError("glBindTexture");

		mTextureCoordsHandleText = GLES20.glGetAttribLocation(mProgramText,
				"aTexCoords");
		SliderRenderer.checkGlError("glGetAttribLocation");

		GLES20.glEnableVertexAttribArray(mTextureCoordsHandleText);
		SliderRenderer.checkGlError("glEnableVertexAttribArray");

		GLES20.glVertexAttribPointer(mTextureCoordsHandleText,
				COORDS_PER_TEXTURE_VERTEX, GLES20.GL_FLOAT, false,
				TEXTURE_VERTEX_STRIDE_BYTES, textureCoordsBufferText);
		SliderRenderer.checkGlError("glVertexAttribPointer");

		mTextureHandleText = GLES20.glGetUniformLocation(mProgramText,
				"uTexture");
		SliderRenderer.checkGlError("glGetUniformLocation");

		GLES20.glUniform1i(mTextureHandleText, 0);
		SliderRenderer.checkGlError("glUniform1i");

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		SliderRenderer.checkGlError("glActiveTexture");

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandlesText[0]);
		GLUtil.checkGlError("glBindTexture");
		// Set the alpha
		mAlphaHandleText = GLES20.glGetUniformLocation(mProgramText, "uAlpha");
		SliderRenderer.checkGlError("glGetUniformLocation");
		GLES20.glUniform1f(mAlphaHandleText, 1.0f);
		SliderRenderer.checkGlError("glUniform1f");

		// get handle to shape's transformation matrix
		mMVPMatrixHandleText = GLES20.glGetUniformLocation(mProgramText,
				"uMVPMatrix");
		SliderRenderer.checkGlError("glGetUniformLocation");

		// Apply the projection and view transformation
		GLES20.glUniformMatrix4fv(mMVPMatrixHandleText, 1, false, mvpMatrix, 0);
		SliderRenderer.checkGlError("glUniformMatrix4fv");

		// Draw the square
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
				GLES20.GL_UNSIGNED_SHORT, drawListBufferText);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPositionHandleText);

		// GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		// GLUtil.checkGlError("glDeleteTextures");
	}

	public static void onSurfaceCreated(EGLConfig config) {
		Bitmap bitmap = TextRenderer.getBitmap();
		mTextureHandlesText = new int[1];
		mTextureHandlesText[0] = GLUtil.loadTexture(bitmap);
		mBitmap = bitmap;
	}
}