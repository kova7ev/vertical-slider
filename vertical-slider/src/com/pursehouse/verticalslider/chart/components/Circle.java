/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pursehouse.verticalslider.chart.components;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;

import com.pursehouse.verticalslider.SliderRenderer;
import com.pursehouse.verticalslider.chart.utils.ColorHelper;
import com.pursehouse.verticalslider.chart.utils.GLUtil;
import com.pursehouse.verticalslider.chart.utils.MathHelper;
import com.pursehouse.verticalslider.chart.utils.OpenGLHelper;
import com.pursehouse.verticalslider.text.TextRenderer;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.opengl.GLES20;
import android.util.Log;

/**
 * A two-dimensional triangle for use as a drawn object in OpenGL ES 2.0.
 */
public class Circle{

	public static final String TAG = "Strokes";

	private final String vertexShaderCodeStroke =
	// This matrix member variable provides a hook to manipulate
	// the coordinates of the objects that use this vertex shader
	"uniform mat4 uMVPMatrix;" + "attribute vec4 vPosition;" + "void main() {" +
	// the matrix must be included as a modifier of gl_Position
	// Note that the uMVPMatrix factor *must be first* in order
	// for the matrix multiplication product to be correct.
			"  gl_Position = uMVPMatrix * vPosition;" + "}";

	private final String fragmentShaderCodeStroke = "precision mediump float;"
			+ "uniform vec4 vColor;" + "void main() {"
			+ "  gl_FragColor = vColor;" + "}";

	private FloatBuffer vertexBuffer;
	private ShortBuffer drawListBuffer;

	private int mProgram;
	private int mPositionHandle;
	private int mColorHandle;
	private int mMVPMatrixHandle;

	 private final String vertexShaderCode =
	            // This matrix member variable provides a hook to manipulate
	            // the coordinates of the objects that use this vertex shader
	            "uniform mat4 uMVPMatrix;" +
	            "attribute vec4 vPosition;" +
	            "void main() {" +
	            // the matrix must be included as a modifier of gl_Position
	            // Note that the uMVPMatrix factor *must be first* in order
	            // for the matrix multiplication product to be correct.
	            "  gl_Position = uMVPMatrix * vPosition;" +
	            "}";

	private final String fragmentShaderCode = "precision mediump float;"
			+ "uniform vec4 vColor;" + "void main() {"
			+ "  gl_FragColor = vColor;" + "}";


		// number of coordinates per vertex in this array
	static final int COORDS_PER_VERTEX = 3;
	static final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
	// S, T (or X, Y)
	static final int COORDS_PER_TEXTURE_VERTEX = 2;
	static final int TEXTURE_VERTEX_STRIDE_BYTES = COORDS_PER_TEXTURE_VERTEX
			* GLUtil.BYTES_PER_FLOAT;
	/*
	 * static float triangleCoords[] = { // in counterclockwise order: 0.0f,
	 * 0.622008459f, 0.0f, // top -0.5f, -0.311004243f, 0.0f, // bottom left
	 * 0.5f, -0.311004243f, 0.0f // bottom right };
	 */

	private float vertexCoords[];
	private float textureCoords[];
	private short drawOrder[]; // order to draw
								// vertices

	//	float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 0.0f };

	private float mX;
	private float mY;
	private float mRadius;
	private float mDxRadius;
	private float mDxAngle;
	private float mColor[];
	
	public Circle(float x, float y, float radius, String color) {
		this(x, y, radius, ColorHelper.hexToColor(color));
	}
	
	public Circle(float x, float y, float radius, float[] color) {
		mX = x;
		mY = y;
		mRadius = 0;
		mDxRadius = radius;
		mDxAngle = 1;
		mColor = color;
		// prepare shaders and OpenGL program
		int vertexShader = OpenGLHelper.loadShader(GLES20.GL_VERTEX_SHADER,
				vertexShaderCodeStroke);
		int fragmentShader = OpenGLHelper.loadShader(
				GLES20.GL_FRAGMENT_SHADER, fragmentShaderCodeStroke);

		mProgram = GLES20.glCreateProgram(); // create empty OpenGL
													// Program
		GLES20.glAttachShader(mProgram, vertexShader); // add the vertex
																// shader
		// to program
		GLES20.glAttachShader(mProgram, fragmentShader); // add the
																// fragment
																// shader to
																// program
		GLES20.glLinkProgram(mProgram); // create OpenGL program
												// executables

		// prepare shaders and OpenGL program
		vertexShader = SliderRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
				vertexShaderCode);
		fragmentShader = SliderRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
				fragmentShaderCode);

		mProgram = GLES20.glCreateProgram(); // create empty OpenGL
													// Program
		GLES20.glAttachShader(mProgram, vertexShader); // add the vertex
															// shader
		// to program
		GLES20.glAttachShader(mProgram, fragmentShader); // add the
																// fragment
																// shader to
																// program
		GLES20.glLinkProgram(mProgram); // create OpenGL program
											// executables
	}
	
	public void draw(float[] mvpMatrix) {
		// Add program to OpenGL environment
				GLES20.glUseProgram(mProgram);

				int numberRect = (int)(360 / mDxAngle);
				vertexCoords = new float[numberRect * 4 * 3];
				int index = 0;

				for (int i = 0; i < numberRect; i++){
					
					//V0
					float angle = mDxAngle * i;
					float y = mY + (float)(mRadius * Math.sin(MathHelper.degreeToRad(angle)));
					float x = mX + (float)(mRadius * Math.cos(MathHelper.degreeToRad(angle)));
					
					vertexCoords[index] = x;
					vertexCoords[index + 1] = y;
					vertexCoords[index + 2] = 0.0f;
					
					index += 3;
					
					//V1
					y = mY + (float)(mRadius * Math.sin(MathHelper.degreeToRad(angle + mDxAngle)));
					x = mX + (float)(mRadius * Math.cos(MathHelper.degreeToRad(angle + mDxAngle)));
					
					vertexCoords[index] = x;
					vertexCoords[index + 1] = y;
					vertexCoords[index + 2] = 0.0f;
					
					index += 3;
					
					//V2
					y = mY + (float)((mRadius+mDxRadius) * Math.sin(MathHelper.degreeToRad(angle + mDxAngle)));
					x = mX + (float)((mRadius+mDxRadius) * Math.cos(MathHelper.degreeToRad(angle + mDxAngle)));
					
					vertexCoords[index] = x;
					vertexCoords[index + 1] = y;
					vertexCoords[index + 2] = 0.0f;
					
					index += 3;
					
					//V3
					y = mY + (float)((mRadius+mDxRadius) * Math.sin(MathHelper.degreeToRad(angle)));
					x = mX + (float)((mRadius+mDxRadius) * Math.cos(MathHelper.degreeToRad(angle)));
					
					vertexCoords[index] = x;
					vertexCoords[index + 1] = y;
					vertexCoords[index + 2] = 0.0f;
					
					index += 3;
				}
				
				drawOrder = new short[numberRect * 6];
				index = 0;

				for (int i = 0; i < numberRect; i++) {
					// { 0, 1, 2, 0, 2, 3 }
					drawOrder[index] = (short) (i * 4);
					drawOrder[index + 1] = (short) (i * 4 + 1);
					drawOrder[index + 2] = (short) (i * 4 + 2);
					drawOrder[index + 3] = (short) (i * 4);
					drawOrder[index + 4] = (short) (i * 4 + 2);
					drawOrder[index + 5] = (short) (i * 4 + 3);

					index += 6;
				}

				// initialize vertex byte buffer for shape coordinates
				ByteBuffer bb = ByteBuffer.allocateDirect(
				// (number of coordinate values * 4 bytes per float)
						vertexCoords.length * 4);
				// use the device hardware's native byte order
				bb.order(ByteOrder.nativeOrder());

				// create a floating point buffer from the ByteBuffer
				vertexBuffer = bb.asFloatBuffer();
				// add the coordinates to the FloatBuffer
				vertexBuffer.put(vertexCoords);
				// set the buffer to read the first coordinate
				vertexBuffer.position(0);

				// initialize byte buffer for the draw list
				ByteBuffer dlb = ByteBuffer.allocateDirect(
				// (# of coordinate values * 2 bytes per short)
						drawOrder.length * 2);
				dlb.order(ByteOrder.nativeOrder());
				drawListBuffer = dlb.asShortBuffer();
				drawListBuffer.put(drawOrder);
				drawListBuffer.position(0);

				// get handle to vertex shader's vPosition member
				mPositionHandle = GLES20.glGetAttribLocation(mProgram,
						"vPosition");

				// Enable a handle to the triangle vertices
				GLES20.glEnableVertexAttribArray(mPositionHandle);

				// Prepare the triangle coordinate data
				GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
						GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);

				// get handle to fragment shader's vColor member
				mColorHandle = GLES20.glGetUniformLocation(mProgram,
						"vColor");

				// Set color for drawing the triangle
				GLES20.glUniform4fv(mColorHandle, 1, mColor, 0);

				// get handle to shape's transformation matrix
				mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram,
						"uMVPMatrix");
				SliderRenderer.checkGlError("glGetUniformLocation");

				// Apply the projection and view transformation
				GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix,
						0);
				SliderRenderer.checkGlError("glUniformMatrix4fv");

				// Draw the square
				GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
						GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

				// Disable vertex array
				GLES20.glDisableVertexAttribArray(mPositionHandle);

	}
}