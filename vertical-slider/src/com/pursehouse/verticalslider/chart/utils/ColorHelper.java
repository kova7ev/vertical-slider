package com.pursehouse.verticalslider.chart.utils;

public class ColorHelper {
	public static float[] hexToColor(String hex){
		if (hex == null || hex.length() != 7 || !hex.substring(0, 1).equals("#")){
			return null;
		}
		
		float r = 1.0f * Integer.parseInt(hex.substring(1, 3), 16) / 255;
		float g = 1.0f * Integer.parseInt(hex.substring(3, 5), 16) / 255;
		float b = 1.0f * Integer.parseInt(hex.substring(5, 7), 16) / 255;
		return new float[]{r, g, b, 1.0f};
	}
}
