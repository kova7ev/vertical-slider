/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pursehouse.verticalslider;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;

import com.pursehouse.verticalslider.chart.utils.GLUtil;
import com.pursehouse.verticalslider.text.Text;
import com.pursehouse.verticalslider.text.TextRenderer;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.opengl.GLES20;
import android.util.Log;

/**
 * A two-dimensional triangle for use as a drawn object in OpenGL ES 2.0.
 */
public class Strokes {

	public static final String TAG = "Strokes";

	private final String vertexShaderCodeStroke =
	// This matrix member variable provides a hook to manipulate
	// the coordinates of the objects that use this vertex shader
	"uniform mat4 uMVPMatrix;" + "attribute vec4 vPosition;" + "void main() {" +
	// the matrix must be included as a modifier of gl_Position
	// Note that the uMVPMatrix factor *must be first* in order
	// for the matrix multiplication product to be correct.
			"  gl_Position = uMVPMatrix * vPosition;" + "}";

	private final String fragmentShaderCodeStroke = "precision mediump float;"
			+ "uniform vec4 vColor;" + "void main() {"
			+ "  gl_FragColor = vColor;" + "}";

	private FloatBuffer vertexBufferStroke;
	private ShortBuffer drawListBufferStroke;

	private int mProgramStroke;
	private int mPositionHandleStroke;
	private int mColorHandleStroke;
	private int mMVPMatrixHandleStroke;

	private final String vertexShaderCodeText = ""
			+
			// This matrix member variable provides a hook to manipulate
			// the coordinates of the objects that use this vertex shader
			"uniform mat4 uMVPMatrix;" + "attribute vec4 aPosition;"
			+ "attribute vec2 aTexCoords;" + "varying vec2 vTexCoords;"
			+ "void main(){" + "  vTexCoords = aTexCoords;"
			+ "  gl_Position = uMVPMatrix * aPosition;" + "}";

	private final String fragmentShaderCodeText = ""
			+ "precision mediump float;" + "uniform sampler2D uTexture;"
			+ "uniform float uAlpha;" + "varying vec2 vTexCoords;"
			+ "void main(){"
			+ "  gl_FragColor = texture2D(uTexture, vTexCoords);"
			// + "  gl_FragColor.a = uAlpha;"
			+ "}";

	private FloatBuffer vertexBufferText;
	private ShortBuffer drawListBufferText;
	private FloatBuffer textureCoordsBufferText;
	private static int mProgramText;
	// private static boolean mProgramCreated;
	private int mPositionHandleText;
	// private int mColorHandle;
	private int mAlphaHandleText;
	private int mTextureHandleText;
	private int mTextureCoordsHandleText;
	private int mMVPMatrixHandleText;
	private static int mTextureHandlesText[];

	private static Bitmap mBitmap;

	// number of coordinates per vertex in this array
	static final int COORDS_PER_VERTEX = 3;
	static final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
	// S, T (or X, Y)
	static final int COORDS_PER_TEXTURE_VERTEX = 2;
	static final int TEXTURE_VERTEX_STRIDE_BYTES = COORDS_PER_TEXTURE_VERTEX
			* GLUtil.BYTES_PER_FLOAT;
	/*
	 * static float triangleCoords[] = { // in counterclockwise order: 0.0f,
	 * 0.622008459f, 0.0f, // top -0.5f, -0.311004243f, 0.0f, // bottom left
	 * 0.5f, -0.311004243f, 0.0f // bottom right };
	 */

	private float vertexCoords[];
	private float textureCoords[];
	private short drawOrder[]; // order to draw
								// vertices

	// float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 0.0f };
	float color[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	// private int mMinValue;
	// private int mMaxValue;
	private int mNumberItems;
	private float mDh;
	private List<Stroke> mStrokes;
	private List<Text> mLabels;
	private Text mValue;

	// private Text mText;

	public Strokes(int minValue, int maxValue) {
		this(minValue, maxValue, 101);
	}

	public Strokes(int minValue, int maxValue, int numberItems) {
		// mMinValue = minValue;
		// mMaxValue = maxValue;
		mNumberItems = numberItems;
		mStrokes = new ArrayList<Stroke>();
		mLabels = new ArrayList<Text>();
		
		mValue = new Text(0, 0, "0");
		mValue.setScale(3);
		
		// TODO: pass as arguments
		float strokeWidth = 2.0f / 50;
		float strokeHeight = 2.0f / 250;
		float marginTop = 2.0f / 40;
		float marginBottom = 2.0f / 40;

		float dh = (2.0f - marginTop - marginBottom) / (mNumberItems + 1);
		mDh = dh;

		for (int i = 0; i < mNumberItems; i++) {
			float posY = -1.0f + marginTop + (i+1) * dh;

			Stroke stroke = new Stroke(strokeWidth, strokeHeight, posY);
			Text label = null;//

			if (i % 5 == 0)
			{
				float posX = 1.0f - 1.5f * strokeWidth;
				label = new Text(posX, posY, String.format("%d", i));
				label.setScale(0);
			}else{
				stroke.setVisible(false);
			}

			mStrokes.add(stroke);
			mLabels.add(label);
		}

		// prepare shaders and OpenGL program
		int vertexShader = SliderRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
				vertexShaderCodeStroke);
		int fragmentShader = SliderRenderer.loadShader(
				GLES20.GL_FRAGMENT_SHADER, fragmentShaderCodeStroke);

		mProgramStroke = GLES20.glCreateProgram(); // create empty OpenGL
													// Program
		GLES20.glAttachShader(mProgramStroke, vertexShader); // add the vertex
																// shader
		// to program
		GLES20.glAttachShader(mProgramStroke, fragmentShader); // add the
																// fragment
																// shader to
																// program
		GLES20.glLinkProgram(mProgramStroke); // create OpenGL program
												// executables

		// prepare shaders and OpenGL program
		vertexShader = SliderRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
				vertexShaderCodeText);
		fragmentShader = SliderRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
				fragmentShaderCodeText);

		mProgramText = GLES20.glCreateProgram(); // create empty OpenGL
													// Program
		GLES20.glAttachShader(mProgramText, vertexShader); // add the vertex
															// shader
		// to program
		GLES20.glAttachShader(mProgramText, fragmentShader); // add the
																// fragment
																// shader to
																// program
		GLES20.glLinkProgram(mProgramText); // create OpenGL program
											// executables
	}

	/**
	 * Encapsulates the OpenGL ES instructions for drawing this shape.
	 *
	 * @param mvpMatrix
	 *            - The Model View Project matrix in which to draw this shape.
	 */
	public void draw(float[] mvpMatrix) {
		drawStrokes(mvpMatrix);
		drawLabels(mvpMatrix);
		//mValue.draw(mvpMatrix);
	}

	public float getDh() {
		return mDh;
	}

	// public void update(float y) {
	// // TODO: pass as arguments
	// float strokeWidth = 2.0f / 50;
	// float strokeHeight = 2.0f / 250;
	// float marginTop = 2.0f / 40;
	// float marginBottom = 2.0f / 40;
	//
	// float dh = (2.0f - marginTop - marginBottom) / (mNumberItems + 1);
	//
	// float k = 2 * (0.9f + 0.7f + 0.4f) * 0.17f + 0.17f;
	// int index = (int) ((y + 1) / dh);
	//
	// dh = (2.0f - marginTop - marginBottom - k) / (mNumberItems + 1 - 8);
	//
	// for (int i = 0; i < mStrokes.size(); i++) {
	// float posY = -1.0f + marginTop + (i+1) * dh;
	// Stroke stroke = mStrokes.get(i);
	//
	// stroke.setWidth(strokeWidth);
	// stroke.setHeight(strokeHeight);
	// stroke.setPositionY(i >= index + 4 ? posY + k : posY);
	//
	// if (i > index - 4 && i < index + 4){
	// stroke.setScale(0);
	// }else{
	// stroke.setScale(1);
	// }
	// }
	//
	// for (int i = 1; i < 2; i++){
	// //float posY = -1.0f + marginTop + (i+1) * dh;
	// Stroke strokeRight = mStrokes.get(index + i);
	// Stroke strokeLeft = mStrokes.get(index - i);
	//
	// strokeRight.setWidth(strokeWidth);
	// strokeRight.setHeight(strokeHeight);
	// strokeRight.setPositionY(y + 0.17f);
	// strokeRight.setScale(1);
	//
	// strokeLeft.setWidth(strokeWidth);
	// strokeLeft.setHeight(strokeHeight);
	// strokeLeft.setPositionY(y - 0.17f);
	// strokeLeft.setScale(1);
	// }
	// }

	public void update(float y) {
		if (mValue != null){
			mValue.setPositionY(y);
		}

		// TODO: pass as arguments
		float strokeWidth = 2.0f / 50;
		float strokeHeight = 2.0f / 250;
		float marginTop = 2.0f / 40;
		float marginBottom = 2.0f / 40;

		float dh = (2.0f - marginTop - marginBottom) / (mNumberItems + 1);
		float dH = 2.0f / 7;

		// float k = (0.9f + 0.7f + 0.4f) * 0.17f;
		// float dhl = y - k;
		// float dhr = y + k;

		float minDistance = Float.MAX_VALUE;
		int index = -1;
		
		for (int i = 0; i < mStrokes.size(); i++) {
			float posY = -1.0f + marginTop + (i + 1) * dh;
			Stroke stroke = mStrokes.get(i);
			Text label = mLabels.get(i);

			if (Math.abs(posY - y) < dH) {
				// mText.setText(Float.toString(1.0f * i + (posY-y)/dH));
				float scale = 1 - (Math.max(posY, y) - Math.min(posY, y)) / dH;
				float dy = posY - y;
				
				
				if (Math.abs(dy) < minDistance){
					minDistance = Math.abs(dy);
					index = i;
				}
				
				//Log.w(TAG, String.format("f(x): %f, dy: %f, y: %f", Math.signum(dy) * dH * Math.sqrt(Math.abs(dy/dH)), dy, y));
				
				float kernel = (float) (Math.signum(dy) * Math.abs(dy/dH));
				//mValue.setText(String.format("%f", 0.5f * (kernel + 1)));
				
				//if (mValue != null){
				//	mValue.setText(String.format("%d", i));
				//}
				
				posY = (float) (posY + Math.signum(dy) * (dH  - dH * Math.sqrt(1 - Math.abs(dy/dH))));

				stroke.setScale(scale < 0.33f ? 1 : scale * 3);
				//stroke.setScale(scale * 3);
				
				if (label != null) {
					//label.setScale(scale < 0.33f ? 1 : scale * 3);
					label.setScale(scale * 2);
				}
			} else {
				if (posY > y) {
					posY += dH;
				} else if (posY < y) {
					posY -= dH;
				}

				stroke.setScale(1);
				if (label != null) {
					label.setScale(0);
				}
			}

			// if (posY < dhr && posY > dhl){
			// float scale = (1 - Math.abs(y - posY) / k) * 5;
			// stroke.setScale(scale < 1 ? 1 : scale);
			// }else{
			// stroke.setScale(1);
			// }

			stroke.setWidth(strokeWidth);
			stroke.setHeight(strokeHeight);
			stroke.setPositionY(posY);
			//stroke.getText().setPositionX();
			
			if (label != null){
				label.setPositionY(
						posY);
			}

			// if ( (i-1)%10 == 0){
			// stroke.setText(String.format("%d", (i/10)+1));
			// }
		}
		
		if (mValue != null){
			mValue.setText(String.format("%d", index));
		}
	}

	private void drawStrokes(float[] mvpMatrix) {
		// Add program to OpenGL environment
		GLES20.glUseProgram(mProgramStroke);

		vertexCoords = new float[mStrokes.size() * 4 * 3];
		int index = 0;

		for (int i = 0; i < mStrokes.size(); i++) {
			Stroke stroke = mStrokes.get(i);

			float strokePosY = stroke.getPositionY();
			float strokeHeight = stroke.getHeight();
			float strokeWidth = stroke.getWidth();
			float strokeScale = stroke.getScale();
			
			if (!stroke.getVisible()){
				strokeScale = 0;
			}

			vertexCoords[index] = 1.0f;
			vertexCoords[index + 1] = strokePosY + strokeHeight / 2.0f;
			vertexCoords[index + 2] = 0.0f;

			vertexCoords[index + 3] = 1.0f;
			vertexCoords[index + 4] = strokePosY - strokeHeight / 2.0f;
			vertexCoords[index + 5] = 0.0f;

			vertexCoords[index + 6] = 1.0f - strokeScale * strokeWidth;
			vertexCoords[index + 7] = strokePosY - strokeHeight / 2.0f;
			vertexCoords[index + 8] = 0.0f;

			vertexCoords[index + 9] = 1.0f - strokeScale * strokeWidth;
			vertexCoords[index + 10] = strokePosY + strokeHeight / 2.0f;
			vertexCoords[index + 11] = 0.0f;

			index += 12;
		}

		drawOrder = new short[mStrokes.size() * 6];
		index = 0;

		for (int i = 0; i < mStrokes.size(); i++) {
			// { 0, 1, 2, 0, 2, 3 }
			drawOrder[index] = (short) (i * 4);
			drawOrder[index + 1] = (short) (i * 4 + 1);
			drawOrder[index + 2] = (short) (i * 4 + 2);
			drawOrder[index + 3] = (short) (i * 4);
			drawOrder[index + 4] = (short) (i * 4 + 2);
			drawOrder[index + 5] = (short) (i * 4 + 3);

			index += 6;
		}

		// initialize vertex byte buffer for shape coordinates
		ByteBuffer bb = ByteBuffer.allocateDirect(
		// (number of coordinate values * 4 bytes per float)
				vertexCoords.length * 4);
		// use the device hardware's native byte order
		bb.order(ByteOrder.nativeOrder());

		// create a floating point buffer from the ByteBuffer
		vertexBufferStroke = bb.asFloatBuffer();
		// add the coordinates to the FloatBuffer
		vertexBufferStroke.put(vertexCoords);
		// set the buffer to read the first coordinate
		vertexBufferStroke.position(0);

		// initialize byte buffer for the draw list
		ByteBuffer dlb = ByteBuffer.allocateDirect(
		// (# of coordinate values * 2 bytes per short)
				drawOrder.length * 2);
		dlb.order(ByteOrder.nativeOrder());
		drawListBufferStroke = dlb.asShortBuffer();
		drawListBufferStroke.put(drawOrder);
		drawListBufferStroke.position(0);

		// get handle to vertex shader's vPosition member
		mPositionHandleStroke = GLES20.glGetAttribLocation(mProgramStroke,
				"vPosition");

		// Enable a handle to the triangle vertices
		GLES20.glEnableVertexAttribArray(mPositionHandleStroke);

		// Prepare the triangle coordinate data
		GLES20.glVertexAttribPointer(mPositionHandleStroke, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, vertexStride, vertexBufferStroke);

		// get handle to fragment shader's vColor member
		mColorHandleStroke = GLES20.glGetUniformLocation(mProgramStroke,
				"vColor");

		// Set color for drawing the triangle
		GLES20.glUniform4fv(mColorHandleStroke, 1, color, 0);

		// get handle to shape's transformation matrix
		mMVPMatrixHandleStroke = GLES20.glGetUniformLocation(mProgramStroke,
				"uMVPMatrix");
		SliderRenderer.checkGlError("glGetUniformLocation");

		// Apply the projection and view transformation
		GLES20.glUniformMatrix4fv(mMVPMatrixHandleStroke, 1, false, mvpMatrix,
				0);
		SliderRenderer.checkGlError("glUniformMatrix4fv");

		// Draw the square
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
				GLES20.GL_UNSIGNED_SHORT, drawListBufferStroke);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPositionHandleStroke);
	}

	private void drawLabels(float[] mvpMatrix) {
		// Add program to OpenGL environment
		GLES20.glUseProgram(mProgramText);

		int numberCharacters = 0;
		for (int i = 0; i < mLabels.size(); i++) {
			Text text = mLabels.get(i);

			if (text != null) {
				numberCharacters += text.getText().length();
			}
			
			numberCharacters += mValue == null ? 0 : mValue.getText().length();
		}

		vertexCoords = new float[numberCharacters * 4 * COORDS_PER_VERTEX];
		textureCoords = new float[numberCharacters * 4
				* COORDS_PER_TEXTURE_VERTEX];

		int index = 0;
		int vertexCoordOffset = 0;

		for (int i = 0; i < mLabels.size() + (mValue != null ? 1 : 0); i++) {
			Text label = (i == mLabels.size() ? mValue : mLabels.get(i));
			if (label != null) {
				String text = label.getText();

				for (int j = 0; j < text.length(); j++) {
					String c = text.substring(j, j+1);
					
					// TODO: While support string with length == 1
					Rect rect = TextRenderer.getTextureCoords(c);

					if (rect == null) {
						return;
					}

					float left = 1.0f * rect.left / mBitmap.getWidth();
					float top = 1.0f * rect.top / mBitmap.getHeight();
					float right = 1.0f * rect.right / mBitmap.getWidth();
					float bottom = 1.0f * rect.bottom / mBitmap.getHeight();

					textureCoords[index] = left;
					textureCoords[index + 1] = top;
					textureCoords[index + 2] = left;
					textureCoords[index + 3] = bottom;
					textureCoords[index + 4] = right;
					textureCoords[index + 5] = bottom;
					textureCoords[index + 6] = right;
					textureCoords[index + 7] = top;

					index += 8;
					
//					float width = TextRenderer.getWidth()
//							/ (0.5f * Text.getScreenWidth());
//					float height = TextRenderer.getHeight()
//							/ (0.5f * Text.getScreenHeight());
					
					//TODO:
					float width = 0;
					float height = 0;
					
					float scale = label.getScale();
					float posX = label.getPositionX() - scale * width * j  - (i == mLabels.size() ? - mValue.getText().length() * width * 1.5f : mStrokes.get(i).getWidth() * scale);
					float posY = label.getPositionY();

					vertexCoords[vertexCoordOffset] = posX;
					vertexCoords[vertexCoordOffset + 1] = posY + scale * height / 2;
					vertexCoords[vertexCoordOffset + 2] = 0.0f;

					vertexCoords[vertexCoordOffset + 3] = posX;
					vertexCoords[vertexCoordOffset + 4] = posY - scale * height / 2;
					vertexCoords[vertexCoordOffset + 5] = 0.0f;

					vertexCoords[vertexCoordOffset + 6] = posX - scale * width;
					vertexCoords[vertexCoordOffset + 7] = posY - scale * height / 2;
					vertexCoords[vertexCoordOffset + 8] = 0.0f;

					vertexCoords[vertexCoordOffset + 9] = posX - scale * width;
					vertexCoords[vertexCoordOffset + 10] = posY + scale * height / 2;
					vertexCoords[vertexCoordOffset + 11] = 0.0f;

					vertexCoordOffset += 12;

				}

			}
		}

		// initialize vertex byte buffer for shape coordinates
		ByteBuffer bb = ByteBuffer.allocateDirect(
		// (# of coordinate values * 4 bytes per float)
				textureCoords.length * 4).order(ByteOrder.nativeOrder());

		textureCoordsBufferText = bb.asFloatBuffer();
		textureCoordsBufferText.put(textureCoords);
		textureCoordsBufferText.position(0);

		/*
		index = 0;
		for (int i = 0; i < mLabels.size(); i++) {
			Text label = mLabels.get(i);
			if (label != null) {
				float posX = label.getPositionX();
				float posY = label.getPositionY();
				float scale = label.getScale();

				float width = TextRenderer.getWidth()
						/ (0.5f * Text.getScreenWidth());
				float height = TextRenderer.getHeight()
						/ (0.5f * Text.getScreenHeight());

				vertexCoords[index] = posX;
				vertexCoords[index + 1] = posY + scale * height / 2;
				vertexCoords[index + 2] = 0.0f;

				vertexCoords[index + 3] = posX;
				vertexCoords[index + 4] = posY - scale * height / 2;
				vertexCoords[index + 5] = 0.0f;

				vertexCoords[index + 6] = posX - scale * width;
				vertexCoords[index + 7] = posY - scale * height / 2;
				vertexCoords[index + 8] = 0.0f;

				vertexCoords[index + 9] = posX - scale * width;
				vertexCoords[index + 10] = posY + scale * height / 2;
				vertexCoords[index + 11] = 0.0f;

				index += 12;
			}
		}*/

		// initialize vertex byte buffer for shape coordinates
		bb = ByteBuffer.allocateDirect(
		// (# of coordinate values * 4 bytes per float)
				vertexCoords.length * 4);
		bb.order(ByteOrder.nativeOrder());
		vertexBufferText = bb.asFloatBuffer();
		vertexBufferText.put(vertexCoords);
		vertexBufferText.position(0);

		drawOrder = new short[numberCharacters * 6];
		index = 0;

		for (int i = 0; i < numberCharacters; i++) {
			// { 0, 1, 2, 0, 2, 3 }
			drawOrder[index] = (short) (i * 4);
			drawOrder[index + 1] = (short) (i * 4 + 1);
			drawOrder[index + 2] = (short) (i * 4 + 2);
			drawOrder[index + 3] = (short) (i * 4);
			drawOrder[index + 4] = (short) (i * 4 + 2);
			drawOrder[index + 5] = (short) (i * 4 + 3);

			index += 6;
		}

		// initialize byte buffer for the draw list
		ByteBuffer dlb = ByteBuffer.allocateDirect(
		// (# of coordinate values * 2 bytes per short)
				drawOrder.length * 2);
		dlb.order(ByteOrder.nativeOrder());
		drawListBufferText = dlb.asShortBuffer();
		drawListBufferText.put(drawOrder);
		drawListBufferText.position(0);

		// get handle to vertex shader's vPosition member
		mPositionHandleText = GLES20.glGetAttribLocation(mProgramText,
				"aPosition");

		// Enable a handle to the triangle vertices
		GLES20.glEnableVertexAttribArray(mPositionHandleText);

		// Prepare the triangle coordinate data
		GLES20.glVertexAttribPointer(mPositionHandleText, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, vertexStride, vertexBufferText);

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandlesText[0]);
		GLUtil.checkGlError("glBindTexture");

		mTextureCoordsHandleText = GLES20.glGetAttribLocation(mProgramText,
				"aTexCoords");
		SliderRenderer.checkGlError("glGetAttribLocation");

		GLES20.glEnableVertexAttribArray(mTextureCoordsHandleText);
		SliderRenderer.checkGlError("glEnableVertexAttribArray");

		GLES20.glVertexAttribPointer(mTextureCoordsHandleText,
				COORDS_PER_TEXTURE_VERTEX, GLES20.GL_FLOAT, false,
				TEXTURE_VERTEX_STRIDE_BYTES, textureCoordsBufferText);
		SliderRenderer.checkGlError("glVertexAttribPointer");

		mTextureHandleText = GLES20.glGetUniformLocation(mProgramText,
				"uTexture");
		SliderRenderer.checkGlError("glGetUniformLocation");

		GLES20.glUniform1i(mTextureHandleText, 0);
		SliderRenderer.checkGlError("glUniform1i");

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		SliderRenderer.checkGlError("glActiveTexture");

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandlesText[0]);
		GLUtil.checkGlError("glBindTexture");
		// Set the alpha
		mAlphaHandleText = GLES20.glGetUniformLocation(mProgramText, "uAlpha");
		SliderRenderer.checkGlError("glGetUniformLocation");
		GLES20.glUniform1f(mAlphaHandleText, 1.0f);
		SliderRenderer.checkGlError("glUniform1f");

		// get handle to shape's transformation matrix
		mMVPMatrixHandleText = GLES20.glGetUniformLocation(mProgramText,
				"uMVPMatrix");
		SliderRenderer.checkGlError("glGetUniformLocation");

		// Apply the projection and view transformation
		GLES20.glUniformMatrix4fv(mMVPMatrixHandleText, 1, false, mvpMatrix, 0);
		SliderRenderer.checkGlError("glUniformMatrix4fv");

		// Draw the square
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
				GLES20.GL_UNSIGNED_SHORT, drawListBufferText);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPositionHandleText);

		// GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		// GLUtil.checkGlError("glDeleteTextures");
	}

	public static void onSurfaceCreated(EGLConfig config) {
		Bitmap bitmap = TextRenderer.getBitmap();
		mTextureHandlesText = new int[1];
		mTextureHandlesText[0] = GLUtil.loadTexture(bitmap);
		mBitmap = bitmap;
	}
}