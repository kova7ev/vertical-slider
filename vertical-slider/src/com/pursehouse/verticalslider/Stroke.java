/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pursehouse.verticalslider;

import com.pursehouse.verticalslider.text.Text;

public class Stroke {

	public static final String TAG = "Stroke";

	private float mWidth;
	private float mHeight;
	private float mColor[];
	private float mScale;
	private float mPosY;
	private Text mText;
	private boolean mDrawText;
	
	public Stroke(float width, float height, float posY) {
		mWidth = width;
		mHeight = height;
		mColor = new float[] { 1.0f, 1.0f, 1.0f, 0.7f };
		mScale = 1.0f;
		mPosY = posY;
		mText = new Text(0, 0, "");
		mDrawText = false;
	}
	
	public void draw(float[] mvpMatrix) {
		if (mDrawText){
			mText.draw(mvpMatrix);
		}
	}

	public void setWidth(float width) {
		mWidth = width;
	}
	
	public float getWidth(){
		return mWidth;
	}

	public void setHeight(float height) {
		mHeight = height;
	}
	
	public float getHeight(){
		return mHeight;
	}

	public void setPositionY(float posY) {
		mPosY = posY;
	}
	
	public float getPositionY(){
		return mPosY;
	}

	public void setColor(float color[]) {
		mColor = color;
	}
	
	public float[] getColor(){
		return mColor;
	}

	public void setScale(float scale) {
		mScale = scale;
	}
	
	public float getScale(){
		return mScale;
	}
	
	public static void onSurfaceChanged(int width, int height) {
		
	}

	boolean visible = true;
	
	public void setVisible(boolean b) {
		visible = b;
	}

	public boolean getVisible() {
		return visible;
	}
}